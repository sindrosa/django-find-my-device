"""
    Will be called from "update_fmdserver_files.sh"
"""
import json
import shutil
import subprocess
from pathlib import Path, PosixPath
from urllib.parse import urlparse

from bs4 import BeautifulSoup
from bx_py_utils.path import assert_is_dir, assert_is_file

import findmydevice


BASE_PATH = Path(findmydevice.__file__).parent
assert_is_dir(BASE_PATH)

FMD_WEB_PATH = BASE_PATH / 'web'
assert_is_dir(FMD_WEB_PATH)

EXTERNAL_DIR_NAME = 'fmd_externals'
STATIC_EXTERNAL_PATH = BASE_PATH / 'static' / EXTERNAL_DIR_NAME
assert_is_dir(STATIC_EXTERNAL_PATH)

# TODO: https://gitlab.com/jedie/django-find-my-device/-/issues/7
STATIC_URL_PREFIX = f'./static/{EXTERNAL_DIR_NAME}'

EXTERNAL_FILE_INFO = STATIC_EXTERNAL_PATH / 'file_info.json'

LEAFLET_IMAGE_BASE_URL = 'https://unpkg.com/leaflet@1.6.0/dist/images/'
LEAFLET_IMAGES = (
    'layers-2x.png',
    'layers.png',
    'marker-icon-2x.png',
    'marker-icon.png',
    'marker-shadow.png',
)


class FilePatcher:
    def __init__(self, file_path: Path):
        print('_' * 100)
        assert_is_file(file_path)
        self.file_path = file_path

    def __enter__(self):
        self.content = self.file_path.read_text(encoding='utf-8')
        return self

    def patch(self, old, new):
        assert old in self.content, f'Error: {old!r} not found in "{self.file_path}" !'
        self.content = self.content.replace(old, new)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            raise
        self.file_path.write_text(self.content, encoding='utf-8')
        print(f' *** {self.file_path} patched ***')
        print('-' * 100)


def download_file(external_file_data, url, file_name):
    file_path = STATIC_EXTERNAL_PATH / file_name
    file_path.unlink(missing_ok=True)

    subprocess.check_call(['wget', url], cwd=STATIC_EXTERNAL_PATH)
    assert file_path.stat().st_size > 0, f'Empty file: "{file_path}" from "{url}" !'

    file_meta = make_file_meta(url, file_name)
    external_file_data.append(file_meta)


def move_fmd_files(external_file_data):
    def move(external_file_data, src_path, target_path):
        print(f'Move "{src_path}" -> "{target_path}"...', end='')
        if src_path.is_dir():
            meta = {
                'item': f'{src_path.name}/*',
                'url': 'https://gitlab.com/Nulide/findmydeviceserver/',
            }
            shutil.rmtree(target_path)
        elif src_path.is_file():
            meta = {
                'file_name': src_path.name,
                'url': 'https://gitlab.com/Nulide/findmydeviceserver/',
            }
            target_path.unlink(missing_ok=True)
        else:
            raise AssertionError(f'Source path "{src_path}" not found!')

        src_path.rename(target_path)
        print('OK')
        external_file_data.append(meta)

    move(
        external_file_data,
        src_path=FMD_WEB_PATH / 'assets',
        target_path=STATIC_EXTERNAL_PATH / 'assets',
    )
    move(
        external_file_data,
        src_path=FMD_WEB_PATH / 'logic.js',
        target_path=STATIC_EXTERNAL_PATH / 'logic.js',
    )
    move(
        external_file_data,
        src_path=FMD_WEB_PATH / 'style.css',
        target_path=STATIC_EXTERNAL_PATH / 'style.css',
    )


# def patch_logic_js():
#     with FilePatcher(file_path=STATIC_EXTERNAL_PATH / 'logic.js') as patcher:
#         patcher.patch('applicatoin', 'application')


def extract_external_files(content):
    soup = BeautifulSoup(content, parser='html.parser')
    urls = []

    scripts = soup.find_all('script')
    for script in scripts:
        url = script['src']
        if url.startswith('http'):
            urls.append(url)

    links = soup.find_all('link')
    for link in links:
        url = link['href']
        if url.startswith('http'):
            urls.append(url)

    urls.sort()
    return urls


def make_file_meta(url, file_name):
    file_path = STATIC_EXTERNAL_PATH / file_name
    assert_is_file(file_path)

    file_meta = {
        'file_name': file_name,
        'url': url,
        'file_size': file_path.stat().st_size,
    }
    return file_meta


def replace_external_file(external_file_data, patcher, url):
    parse_result = urlparse(url)
    url_path = parse_result.path
    file_name = PosixPath(url_path).name

    patcher.patch(url, f'{STATIC_URL_PREFIX}/{file_name}')

    download_file(external_file_data, url, file_name)


def replace_external_files(external_file_data, patcher):
    urls = extract_external_files(patcher.content)
    for url in urls:
        replace_external_file(external_file_data, patcher, url)


def patch_index_html(external_file_data):
    with FilePatcher(file_path=FMD_WEB_PATH / 'index.html') as patcher:
        # It's not the origin server ;)
        patcher.patch('<p>@Nulide FMDServer</p>', '')

        patcher.patch('./logic.js', f'{STATIC_URL_PREFIX}/logic.js')
        patcher.patch('./style.css?t=123456', f'{STATIC_URL_PREFIX}/style.css')
        patcher.patch('src="assets/', f'src="{STATIC_URL_PREFIX}/assets/')

        replace_external_files(external_file_data, patcher)


def download_leaflet_images(external_file_data):
    for file_name in LEAFLET_IMAGES:
        url = f'{LEAFLET_IMAGE_BASE_URL}{file_name}'
        download_file(external_file_data, url, file_name)


def patch_leaflet_images():
    with FilePatcher(
        file_path=Path(f'findmydevice/static/{EXTERNAL_DIR_NAME}/leaflet.css')
    ) as patcher:
        for file_name in ('marker-icon.png', 'layers.png', 'layers-2x.png'):
            patcher.patch(f'images/{file_name}', file_name)


if __name__ == '__main__':
    external_file_data = []

    move_fmd_files(external_file_data)
    # patch_logic_js() # Currently nothing to patch
    patch_index_html(external_file_data)
    download_leaflet_images(external_file_data)
    patch_leaflet_images()

    with EXTERNAL_FILE_INFO.open('w') as f:
        json.dump(external_file_data, f, sort_keys=True, ensure_ascii=False, indent=4)
