#!/bin/bash

DST="$(pwd)/findmydevice/"

set -x

(
    cd /tmp
    wget --timestamp https://gitlab.com/Nulide/findmydeviceserver/-/archive/master/findmydeviceserver-master.zip

    unzip -u findmydeviceserver-master.zip "findmydeviceserver-master/web/*" -d /tmp/fmdserver/
    cp -rv "/tmp/fmdserver/findmydeviceserver-master/web/" ${DST}
    rm -Rf /tmp/fmdserver/
)

poetry run python update_fmdserver_files.py
