import logging
import sys

from fmd_client.client import (
    FmdClient,
    delete_device,
    get_device_location,
    register_device,
    send_device_location,
)
from fmd_client.data_classes import LocationData


def demo(fmd_server_url, ssl_verify=True):
    fmd_client = FmdClient(
        fmd_server_url=fmd_server_url,
        ssl_verify=ssl_verify,
    )
    print('_' * 100)
    print('Register device...')
    client_device_data = register_device(fmd_client=fmd_client, plaintext_password='foo')
    print('-' * 100)
    print('Device ID:', client_device_data.short_id)
    print('-' * 100)

    try:
        plain_location = LocationData(
            bat='66',
            lat='52.516265',
            lon='13.37738',
            provider='network',
            date=12346567890,
            encrypted=False,
        )

        print('_' * 100)
        print('Send location:', plain_location)
        send_device_location(
            fmd_client=fmd_client,
            client_device_data=client_device_data,
            location=plain_location,
        )
        print('Location send, ok.')

        print('_' * 100)
        print('Get location...')
        location = get_device_location(
            fmd_client=fmd_client,
            client_device_data=client_device_data,
        )
        print('-' * 100)
        print(location)
        print('-' * 100)
    finally:
        print('_' * 100)
        print('Delete device...')
        if input('\nDelete the device from the Server? (Y/N)').lower() not in ('y', 'j'):
            print('Skip deletion, ok.')
        else:
            delete_device(
                fmd_client=fmd_client,
                client_device_data=client_device_data,
            )
            print('OK')


if __name__ == '__main__':
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.DEBUG,
        # level=logging.INFO,
        format='%(asctime)s %(levelname)s %(name)s %(message)s',
        force=True,
    )

    demo(fmd_server_url='http://0.0.0.0:8008/', ssl_verify=False)

    print('\ndone\n')
