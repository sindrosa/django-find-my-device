from django.apps import AppConfig


class FindMyDeviceConfig(AppConfig):
    name = "findmydevice"
    verbose_name = "Django Find My Device"
